FROM docker.io/debian:bookworm as build

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install build-essential ca-certificates curl git golang jq && \
    v=$(curl -Ls https://api.github.com/repos/harness/drone/releases/latest | jq -r .tag_name) && \
    git clone -b $v https://github.com/harness/drone.git && \
    cd drone && \
    go build -ldflags "-extldflags \"-static\"" -o release/linux/arm/drone-server github.com/drone/drone/cmd/drone-server

FROM docker.io/debian:bookworm
LABEL name="docker-drone"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-drone"
LABEL maintainer="thomas@stewarts.org.uk"

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install ca-certificates

COPY --from=build /drone/release/linux/arm/drone-server /bin
ENTRYPOINT ["/bin/drone-server"]
